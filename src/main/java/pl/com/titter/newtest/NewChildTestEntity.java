/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.com.titter.newtest;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author RENT
 */
@Entity
@Table(name = "new_child_test_entities")
public class NewChildTestEntity extends TestEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "new_column")
    private String newColumn;

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NewChildTestEntity)) {
            return false;
        }
        NewChildTestEntity other = (NewChildTestEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.com.titter.newtest.NewChildTestEntity[ id=" + id + " ]";
    }

    /**
     * @return the newColumn
     */
    public String getNewColumn() {
        return newColumn;
    }

    /**
     * @param newColumn the newColumn to set
     */
    public void setNewColumn(String newColumn) {
        this.newColumn = newColumn;
    }
    
}
