/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.com.titter.testnn.main;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.com.titter.newtest.ChildTestEntity;
import pl.com.titter.newtest.NewChildTestEntity;
import pl.com.titter.newtest.TestEntity;


/**
 *
 * @author RENT
 */
public class NewTestMain {
      public static void main(String[] args){
        SessionFactory instance = ConfigHibernate.getInstance();
        Session session = instance.openSession();
        Transaction t = null;
        try{
            t = session.beginTransaction();
            TestEntity newE = new TestEntity();
            ChildTestEntity cte = new ChildTestEntity();
            NewChildTestEntity ncte = new NewChildTestEntity();
            
            session.save(newE);
            session.save(cte);
            session.save(ncte);
            
//            Query q = session.createQuery("from TestEntity");
//            List<TestEntity> teList = q.list();
//            TestEntity newTE = teList.get(0);
            //session.evict(newE);
            
            newE.setTestValue("testtest");
            
            session.saveOrUpdate(newE);
            
            t.commit();
            System.out.println("SAVED RESERVATION");
        }catch(HibernateException he){
            if (t!=null) t.rollback();
            he.printStackTrace();
            System.out.println("ROLLBACK!");
        }finally{
            session.close();
        }
    }
}
