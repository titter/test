/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.com.titter.testnn.main;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.AliasToBeanResultTransformer;
import pl.com.titter.testnn.Cruise;
import pl.com.titter.testnn.Customer;
import pl.com.titter.testnn.Reservation;
import pl.com.titter.testnn.Ship;

/**
 *
 * @author RENT
 */
public class NewClass {
    public static void main(String[] args){
        SessionFactory instance = ConfigHibernate.getInstance();
        Session session = instance.openSession();
        Transaction t = null;
        try{
            t = session.beginTransaction();
            Reservation r = new Reservation();
            Cruise cruise = new Cruise();
            Ship ship = new Ship();
            
            Customer c = new Customer();
            c.setName("Dominik");
            c.setLastName("Sarnowski");
            
            Customer c1 = new Customer();
            c1.setName("Wiesiek");
            c1.setLastName("Wiesiek");
            
            Customer c2 = new Customer();
            c2.setName("Radek");
            c2.setLastName("Radkowski");

            ship.setName("ATLANTICO");
            ship.setCruises(Arrays.asList(cruise));
            
            cruise.setStartLocation("New York");
            cruise.setDestination("London");
            cruise.setShip(ship);
            //cruise.setReservations(Arrays.asList(r));
            
            r.setCost(BigDecimal.valueOf(2000));
            r.setReservationDate(Date.from(Instant.now()));
            r.setCruise(cruise);
            //r.getCustomers().add(c);
            //r.getCustomers().add(c1);
            //r.getCustomers().add(c2);
            c.getReservations().add(r);
            c1.getReservations().add(r);
            c2.getReservations().add(r);
            
            session.save(r);

            Query q = session.createQuery("FROM Reservation");
            List<Reservation> rsl = q.list();
            rsl.stream().forEach(temp -> {System.out.println(temp.getCruise().getShip().getName());});
            
            rsl.stream().forEach(temp -> {
                temp.getCustomers().forEach(tmpCustomer -> {
                    System.out.println(tmpCustomer.getLastName());
                });
            });
            
            q = session.createQuery("from Cruise");

            List<Cruise> crs = q.list();
            for(Cruise cr: crs){
                System.out.println(cr.getDestination());
            }
            t.commit();
            System.out.println("SAVED RESERVATION");
        }catch(HibernateException he){
            if (t!=null) t.rollback();
            he.printStackTrace();
            System.out.println("ROLLBACK!");
        }finally{
            session.close();
        }
    }
}
